$(function () {
    demo.control();
});


var demo = function () {

    //手机
    var phoneControl = function () {
        var phoneDom = $('#elevator-mobile-app');
        var cloudDom = $('#cloud');
        //鼠标进场事件
        phoneDom.mouseenter(function () {
            //添加雪碧图进场效果
            if (phoneDom.hasClass('easy-out')) {
                phoneDom.removeClass('easy-out');
            }
            phoneDom.addClass('easy-in');
            //执行完动画开始循环从第九张雪碧图开始循环执行抖腿动作
            setTimeout(function () {
                phoneDom.removeClass('easy-in');
                phoneDom.addClass('easy-over');
            }, 1500);
            //添加对话云朵淡进效果
            if (cloudDom.hasClass('mFadeOut')) {
                cloudDom.removeClass('mFadeOut');
            }
            cloudDom.addClass('mFadeIn');
            cloudDom.css('opacity', '1');
        })
        //鼠标出场事件
        phoneDom.mouseleave(function () {
            //添加雪碧图出场效果
            if (phoneDom.hasClass('easy-in')) {
                phoneDom.removeClass('easy-in');
            }
            if (phoneDom.hasClass('easy-over')) {
                phoneDom.removeClass('easy-over');
            }
            phoneDom.addClass('easy-out');
            //添加云朵淡出效果
            if (cloudDom.hasClass('mFadeIn')) {
                cloudDom.removeClass('mFadeIn');
            }
            cloudDom.addClass('mFadeOut');
            cloudDom.css('opacity', '0');
        })

    }

    //星星

    //金币

    //电视


    return {
        control: function () {
            phoneControl();
        }
    }
}();