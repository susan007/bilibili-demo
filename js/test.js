$(function () {
    test.mcontrol();
})


var test = function () {

    //缓冲区一  （如果想存储多个数据，用java的话将数组换成List或者map）
    var B1;
    //缓冲区二
    var B2;

    //进程总控制
    var control = function () {
        var info = "客户端输入数据";

        //重复执行定时器 每3秒执行一次
        window.setInterval(function () {
            //读取
            read(info);
            //修改
            if (B1) {
                move(B1);
            }
            ;
            //打印
            if (B2) {
                print(B2);
            }
        }, 2000);

    }

//读数据进程
    var read = function (info) {
        //如果info存在且不为空 则将信息存储到B1中
        if (info) {
            B1 = info;
            console.log("111>>>赋值");
        }
    }

//修改数据进程
    var move = function (info) {
        //修改读取的数据
        if (info) {
            info = "你被我修改了";
        }
        //存储到缓存区B2
        B2 = info;
        console.log("222>>>修改");
    }

//打印数据进程
    var print = function (info) {
        // console.log(info);
        console.log("333>>>打印");
    }


    return {
        mcontrol: function () {
            control();
        }
    }
}();


